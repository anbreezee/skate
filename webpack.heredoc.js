module.exports = function(source) {
	if (source === undefined) { return source; }
	source = source.replace(/<<<\{([^]+?)\}>>>/gi, function (str, p1) {
		p1 = p1.replace(/\{\{\s(.*)\s\}\}/gi, '"\' + $1 + \'"');
		p1 = p1.split("\n");
		a1 = '';
		for (var i in p1) { a1 += p1[i].trim(); }
		return "'{" + a1 + "}'";
	});
	return source;
};
