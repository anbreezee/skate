require './index.styl'

(() ->
	div = document.createElement 'div'
	div.style = 'height: 0; width: 0; position: absolute; visibility: hidden;'
	div.setAttribute 'aria-hidden', 'true'

	svg = document.createElementNS 'http://www.w3.org/2000/svg', 'svg'
	svg.setAttribute 'focusable', 'false'

	smb = document.createElementNS 'http://www.w3.org/2000/svg', 'symbol'
	smb.setAttribute 'id', 'ripply-scott'
	smb.setAttribute 'viewBox', '0 0 100 100'

	crc = document.createElementNS 'http://www.w3.org/2000/svg', 'circle'
	crc.setAttribute 'id', 'ripple-shape'
	crc.setAttribute 'cx', '1'
	crc.setAttribute 'cy', '1'
	crc.setAttribute 'r', '1'

	smb.appendChild crc
	svg.appendChild smb
	div.appendChild svg
	document.body.appendChild div
)();

Vue.directive(
	'ripple'
	bind: () ->
		svg = document.createElementNS 'http://www.w3.org/2000/svg', 'svg'
		svg.setAttribute 'class', 'ripple-obj'
		use = document.createElementNS 'http://www.w3.org/2000/svg', 'use'
		use.setAttribute 'class', 'js-ripple'
		use.setAttribute 'height', 100
		use.setAttribute 'width', 100
		use.setAttributeNS 'http://www.w3.org/1999/xlink','href','#ripply-scott'
		svg.appendChild use
		@el.appendChild svg

		show = (rippleEl, event) ->
			x            = event.offsetX
			y            = event.offsetY
			w            = event.target.offsetWidth
			h            = event.target.offsetHeight
			offsetX      = Math.abs( (w / 2) - x )
			offsetY      = Math.abs( (h / 2) - y )
			deltaX       = (w / 2) + offsetX
			deltaY       = (h / 2) + offsetY
			scale_ratio  = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2))
			TweenLite.fromTo(
				rippleEl
				0.75
				{
					x               : x
					y               : y
					transformOrigin : '50% 50%'
					scale           : 0
					opacity         : 1
					ease            : Linear.easeIn
				}
				{
					scale   : scale_ratio
					opacity : 0
				}
			)
		@el.addEventListener 'click', (event) -> show use, event
)
