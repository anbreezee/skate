Vue.directive(
	'bem'
	bind: () ->
		_namespace = if @modifiers.block then '' else 'c-'
		if @el.className.length > 0 then _namespace = ' ' + _namespace
		_block = _namespace + @vm.$options.name
		_element = ''
		_modifier = ''
		_elementPrefix = '__'
		_modifierPrefix = '--'

		s__qa = document.s__qa
		if @vm.qa? then s__qa = @vm.qa
		_qaBlock = ' qa-' + @vm.$options.name

		if @arg
			_bem = @arg.split ','
			_element  = if _bem[0]? then _elementPrefix + _bem[0] else ''
			_modifier = if _bem[1]? then _modifierPrefix + _bem[1] else ''

			if @modifiers.block
				_block    = _namespace      + _bem[0]
				_qaBlock  = 'qa'            + _bem[0]
				_element  = _elementPrefix  + _bem[1]
				_modifier = _modifierPrefix + _bem[2]

				if _bem[1]
					@el.className += _block + _element
					if s__qa and not _bem[2]
						@el.className += _qaBlock + _element
				else
					@el.className += _block
					if s__qa and not _bem[2]
						@el.className += _qaBlock

				if _bem[2]
					@el.className += _block + _element + _modifier
					if s__qa
						@el.className += _qaBlock + _element + _modifier

			else
				if _element
					@el.className += _block + _element
					if s__qa and not _modifier
						@el.className += _qaBlock + _element
				else
					@el.className += _block
					if s__qa and not _modifier
						@el.className += _qaBlock

				if _modifier
					@el.className += _block + _element + _modifier
					if s__qa
						@el.className += _qaBlock + _element + _modifier

		else
			@el.className += _block
			if s__qa then @el.className += _qaBlock
)
