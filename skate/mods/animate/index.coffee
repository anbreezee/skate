require 'gsap/src/minified/TweenLite.min.js'
require 'gsap/src/minified/plugins/CSSPlugin.min.js'
require 'gsap/src/minified/easing/EasePack.min.js'

Vue.transition(
	'toast-expand'
	css : false
	enter : (el, done) ->
		style = window.getComputedStyle el
		top = el.parentElement.className.match(/top/i)
		maxHeight = parseInt(style.height) * 1.5 + 'px'
		TweenLite.fromTo(
			el
			0.4
			{
				opacity    : 0
				y          : if top then -parseInt(maxHeight) else 0
				maxHeight  : '0px'
				ease       : Linear.easeIn
				onComplete : done
			}
			{
				opacity    : 1
				y          : 0
				maxHeight  : maxHeight
			}
		)
	enterCancelled : (el) ->
		TweenLite.killTweensOf el
	leave : (el, done) ->
		style = window.getComputedStyle el
		top = el.parentElement.className.match(/top/i)
		maxHeight = parseInt(style.height) * 1.5 + 'px'
		TweenLite.to(
			el
			0.4
			{
				opacity    : 0
				maxHeight  : '0px'
				ease       : Linear.easeIn
				onComplete : done
				y          : if top then -parseInt(maxHeight) else 0
			}
		)
	leaveCancelled : (el) ->
		TweenLite.killTweensOf el
)
