# Helpers
(() ->
	HTMLElement.prototype.applyAttributes = (attrs) ->
		@setAttribute key, val for own key, val of attrs
		@

	document.createElement = ((create) ->
		() ->
			ret = create.apply this, arguments
			ret.applyAttributes arguments[1] if arguments.length > 1
			ret
	)(document.createElement)

	document.br = (count) ->
		count = 1 if not count?
		appEl.appendChild document.createElement 'br' for [0...count]
)()

# Main
appEl = document.getElementById 'app'

# Toast
toastAttrs =
	'v-ref:toast' : ''
	':parted'     : true
	':fullscreen' : false
	'gravity' : 'right|top'
appEl.appendChild document.createElement 's__toast', toastAttrs

# CTimer
ctimerAttrs =
	'v-ref:ctimer'  : ''
	'radius'        : 50
	# ':animated'     : true
	# 'interval'      : 1000
	# 'color'         : '#FF9800'
	':color'        : '[[0, "#2c3e50"], [0.25, "#2980b9"], [0.5, "#27ae60"], [0.75, "rgba(0,0,0,0)"]]'
	'line-width'    : 20
	':show-counter' : true
	'text-size'     : 24
	'text-color'    : 'same'
appEl.appendChild document.createElement 's__ctimer', ctimerAttrs
document.br()

# Show toast button
buttonAttrs =
	'v-ref:button' : ''
	'v-ripple'     : ''
	'size'         : 'sm'
	'title'        : 'Show toast'
appEl.appendChild document.createElement 's__button', buttonAttrs
document.br 2

# Button
makeButtons = (size) ->
	modes = [ 'default', 'primary', 'success', 'info', 'warning', 'alert' ]
	for mode in modes
		buttonAttrs =
			'v-ripple' : ''
			'title'    : mode
			'mode'     : mode
		buttonAttrs['size'] = size if size?
		appEl.appendChild document.createElement 's__button', buttonAttrs
	document.br 2

makeButtons 'sm'
makeButtons null
makeButtons 'lg'
makeButtons 'xl'

# help-button
appEl.appendChild document.createElement 's__help-button', { 'v-ref:help' : '' }
document.br 2

container = new Vue el: '#app'
container.$refs.help.$on 'onClick', () -> alert 'Info'
container.$refs.button.$on 'onClick', () -> showToast()

sentences = [
	"Mr. and Mrs. Dursley",
	"of number four, Privet Drive",
	"were proud to say that they were perfectly normal",
	"thank you very much. They were the last people",
	"you'd expect to be involved in anything strange or mysterious",
	"because they just didn't hold with such nonsense."
]
index = 0

container.$refs.toast.$on('onShow', (target) -> console.log 'toast show', target.msg)
container.$refs.toast.$on('onHide', (target) -> console.log 'toast hide', target.msg)
showToast = () ->
	toastAttrs =
		msg     : sentences[index++]
		ctimer  : true
		timeout : 5000
		actions : [
			{
				title   : 'ok'
				onClick : (elm, action) ->
					console.log 'toast action'
					action.title = 'GOT IT!'
					setTimeout elm.hide, 500
			},{
				title   : 'more info'
				mode    : 'info'
				onClick : (elm, action) -> console.log 'toast more info'
			},{
				mode    : 'help'
				onClick : (elm, action) -> alert 'info!'
			}
		]
		###
		onShow : (target) -> console.log 'toast show'
		onHide : (target) -> console.log 'toast hide'
		###
	console.log toastAttrs
	container.$refs.toast.show toastAttrs

	index = 0 if index == sentences.length

ctimer = container.$refs.ctimer
ctimer.$on('onStart',  () -> console.log 'CTimer started');
ctimer.$on('onFinish', () -> console.log 'CTimer finished');
ctimer.$on('onTick', (iteration, timeout) -> console.log iteration);
ctimer.start 4
