# Skate
# Component: s__toast

require './index.styl'

ToastMessage = (require '../_extend/').extend
	defaults :
		timeout : 5000
	props: ['qa','msg', 'timeout', 'ctimer', 'actions', 'on-show', 'on-hide']
	template : require './toast-message.jade'
	data : () ->
		visible: true
	ready : () ->
		@onShow @ if @onShow?
		@$parent.atShow @
		if @timeout > 0
			@$refs.ctimer.start (@timeout / 1000) if @ctimer
			setTimeout(
				() => @.hide()
				@timeout
			)
	methods :
		hide : () ->
			@visible = false
			@onHide @ if @onHide?
			@$parent.atHide @
		atActionClick : (index) ->
			@actions[index].onClick @, @actions[index] if @actions[index].onClick?

Vue.component 's__toast', (require '../_extend/').extend
	defaults :
		fullscreen : false
		parted     : true
		gravity    : 'right|top'
	props    : ['qa', 'fullscreen', 'parted', 'gravity']
	handlers : ['on-show', 'on-hide']
	template : require './toast.jade'
	data : () ->
		toasts: []
	computed :
		class : () ->
			block = 'c-' + @$options.name
			cls = {}
			cls[block + '--fullsc'] = @fullscreen
			cls[block + '--normal'] = not @fullscreen
			cls[block + '--parted'] = @parted
			cls[block + '--united'] = not @parted
			cls[block + '--left']   = @gravity.match(/left/i)   if @gravity?
			cls[block + '--center'] = @gravity.match(/center/i) if @gravity?
			cls[block + '--right']  = @gravity.match(/right/i)  if @gravity?
			cls[block + '--top']    = @gravity.match(/top/i)    if @gravity?
			cls[block + '--bottom'] = @gravity.match(/bottom/i) if @gravity?
			cls
	components :
		's__toast-message': ToastMessage
	methods :
		show : (params) ->
			message =
				msg     : params.msg
				timeout : params.timeout
				ctimer  : params.ctimer
				actions : params.actions
				onShow  : params.onShow
				onHide  : params.onHide
			if @gravity.match(/bottom/i)
				@toasts.unshift message
			else
				@toasts.push message
		atShow : (target) ->
			@onShow target if @onShow?
			@$emit 'onShow', target
		atHide : (target) ->
			@onHide target if @onHide?
			@$emit 'onHide', target
