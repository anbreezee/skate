module.exports = Vue.extend

	###
	# If component property isn't specified then VUE skip it in component
	# initialization and not set it as object property.
    #
	# This funcion initializes properties as object property, set its value
	# to null and bind it with `data` object.
	#
	# For example: if we skip ui property, then v.ui will be undefined.
	# But if we use this function, v.ui and v.data.ui will be null.
	###
	vueCustomPrepare : (v) ->
		v.$set(key, null) for own key,val of v._props when not v[key]?

	vueCustomParse : (v) ->
		if v.$el.nodeType != 3
			if getComputedStyle(v.$el).content != '' && getComputedStyle(v.$el).content != 'none'
				content = getComputedStyle(v.$el).content
				content = content.replace /^'(.*)'$/g, '$1'
				content = JSON.parse content
				if typeof content is 'string' then content = JSON.parse content
				for own key,val of content
					if not v[key]?
						if typeof val is 'string' and val.charAt(0) is '!' then v.$set(key, val.substring 1)
						else v.$set(key, val)

	# We can't use querySelectorAll() because of using SVG
	getElementsByClassName : (elements, className) ->
		matches  = []
		traverse = (node) ->
			for i in [0...node.childNodes.length]
				if node.childNodes[i].childNodes.length > 0
					traverse node.childNodes[i]
				if node.childNodes[i].getAttribute && node.childNodes[i].getAttribute 'class'
					if node.childNodes[i].getAttribute('class').split(" ").indexOf(className) >= 0
						matches.push node.childNodes[i]
		traverse elements
		matches

	created : () ->
		@$options.vueCustomPrepare @
		for own key, val of @$options.defaults
			if not @[key]? then @[key] = val

	ready : () ->
		@$options.vueCustomParse @
