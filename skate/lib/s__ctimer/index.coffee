# Skate
# Component: s__ctimer

require './index.styl'

Vue.component 's__ctimer', (require '../_extend/').extend
	els :
		circle : null
		text   : null
	defaults :
		interval : 1000
		animated : true
		timeout  : 5
	props    : ['qa', 'timeout', 'animated', 'interval', 'show-counter', 'color', 'text-color', 'radius', 'line-width', 'text-size']
	template : require './index.jade'
	data : () ->
		iteration : 0
	computed :
		innerRadius : () ->
			val = @radius - @lineWidth / 2
			if val < 0 then 0 else val
		initialOffset : () -> 2 * Math.PI * @innerRadius
		currentColor : () ->
			if typeof @color is 'string' then return @color
			else
				for item of @color
					[key, val] = @color[item]
					break if key > (@iteration - 1) / @timeout
					color = val
				color
		currentTextColor : () ->
			return @currentColor if @textColor is 'same'
			@textColor
		transform : () ->
			"rotate(-90) translate(-" + @radius * 2 + ", 0)"
	ready : () ->
		circle = (@$options.getElementsByClassName @$el, 'circle-counter')[0]
		text   = (@$options.getElementsByClassName @$el, 'text-counter')[0]
		@$options.els.circle = circle
		@$options.els.text   = text
		@$options.els.circle.style.strokeDasharray  = @initialOffset
		@$options.els.circle.style.strokeDashoffset = @initialOffset
	methods :
		start : (timeout) ->
			@timeout   = timeout if timeout?
			@iteration = 0
			circle     = @$options.els.circle
			text       = @$options.els.text
			timer = () =>
				if @iteration <= @timeout
					circle.style.strokeDasharray = @initialOffset
					if @animated and @iteration < @timeout
						circle.style.strokeDashoffset = @initialOffset - ((@iteration + 1) * (@initialOffset / @timeout))
						circle.style.transition = 'all ' + @interval + 'ms linear'
						text.style.transition   = 'all ' + @interval + 'ms linear'
					else
						circle.style.strokeDashoffset = @initialOffset - (@iteration * (@initialOffset / @timeout))
				text.textContent = (@timeout - @iteration) if @showCounter
				if @iteration < @timeout
					@$emit 'onTick', @iteration, @timeout
					@iteration++
					setTimeout timer, @interval
				else
					@$emit 'onFinish'
			@$emit 'onStart'
			setTimeout timer, 10
