# Skate
# Component: s__help-button

require './index.styl'

Vue.component 's__help-button', (require '../_extend/').extend
	props    : ['qa']
	template : require './index.jade'
	methods  :
		atClick : (event) -> @$emit 'onClick'
