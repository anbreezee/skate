# Skate
# Component: s__button

require './index.styl'

Vue.component 's__button', (require '../_extend/').extend
	defaults :
		mode  : 'default'
		size  : 'nm'
		title : 'button'
	props    : ['qa', 'mode', 'size', 'title']
	computed :
		class : () ->
			block = 'c-' + @$options.name
			cls = {}
			cls[block + '--' + @size] = @size if @size
			cls[block + '--' + @mode] = @mode if @mode
			cls
	template : require './index.jade'
	methods  :
		atClick : (event) -> @$emit 'onClick'
