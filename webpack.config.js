const NODE_ENV = process.env.NODE_ENV || 'development';

const path               = require('path');
const webpack            = require('webpack');
const ExtractTextPlugin  = require('extract-text-webpack-plugin');
/* const AssetsPlugin       = require('assets-webpack-plugin'); */
const HtmlWebpackPlugin  = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin  = require('copy-webpack-plugin');

function addHash(template, hash) {
	return template;
	return NODE_ENV == 'production' ?
			(template.replace(/\.[^.]+$/, '.[' + hash + ']$&')) :
			(template + '?hash=[' + hash + ']');
}

/*
const heredoc = function (content) {
	if (content === undefined) { return content; }
	return content.replace(/<<<EOL(.*)EOL;/gi, function (str) {
		str = str.replace(/\{\{\s(.*)\s\}\}/gi, "\"' + $1 + '\"");
		return "'" + str + "'";
	})
}
*/

module.exports = {
	context: path.resolve(__dirname, 'skate'),
	entry: {
		skate:   './lib/index.coffee',
		animate: './mods/animate',
		ripple:  './mods/ripple',
		bemitqa: './mods/bemitqa',
		/* fonts:    './fonts', */
		test:    './test.coffee'
	},
	output: {
		path:           path.resolve(__dirname, 'build/skate'),
		publicPath:    '/skate/',
		filename:      addHash('[name].js', 'chunkhash:6'),
		chunkFilename: addHash('[id].js', 'chunkhash:6'),
		library:       '[name]'
	},
	externals: {
		Vue: 'Vue',
		TweenLite: 'TweenLite'
	},

	watch: NODE_ENV == 'development',
	watchOptions: { aggregateTimeout: 100 },

	/* devtool: NODE_ENV == 'development' ? "source-map" : null, */

	resolve: {
		root: path.resolve(__dirname, "node_modules"),
		extensions: ['', '.js', '.coffee', '.styl']
	},
	resolveLoader: {
		root: path.resolve(__dirname, "node_modules"),
		alias: {
			"heredoc": path.join(__dirname, "./webpack.heredoc.js")
		}
	},

	module: {
		loaders: [{
			test:   /\.coffee$/,
			exclude: /(node_modules|bower_components)/,
			loader: 'coffee'
		}, {
			test:   /\.tpl$/,
			loader: 'text'
		}, {
			test:   /\.jade$/,
			loader: 'apply!jade'
		}, {
			test:   /\.styl$/,
			/*loader: ExtractTextPlugin.extract('style', 'css!autoprefixer?browsers=last 2 versions!stylus?resolve url')*/
			/*loader: ExtractTextPlugin.extract('css!autoprefixer?browsers=last 2 versions!stylus?resolve url')*/
			loader: 'style!css!autoprefixer?browsers=last 2 versions!stylus?resolve url!heredoc'
		}, {
			test:   /\.(png|jpg|svg|ttf|eot|woff|woff2)$/,
			loader: addHash('file?name=[path][name].[ext]', 'hash:6')
		}]
	},

	plugins: [
		new webpack.NoErrorsPlugin(),
		new CleanWebpackPlugin(
			[
				path.resolve(__dirname, 'build')
			], {
				root: path.resolve(__dirname),
				verbose: true,
				dry: false
			}
		),
/*
		new ExtractTextPlugin(addHash('[name].css', 'contenthash:6')),
*/
		new webpack.optimize.CommonsChunkPlugin({
			name: 'common'
		}),
/*
		new AssetsPlugin({
			filename: 'assets.json',
			path:     path.resolve(__dirname, 'public/assets')
		}),
		new HtmlWebpackPlugin({
			filename: '../index.html',
			chunks: ['common', 'animate', 'ripple', 'lib', 'test'],
			chunksSortMode: 'dependency',
			template: '../test/index.html',
			inject: 'body'
		}),
*/

		new CopyWebpackPlugin([
			{ from: '../test/img', to: '../img', toType: 'dir' },
			{ from: '../test/index.html', to: '../index.html' }
		])
	],

};

if (NODE_ENV == 'production') {
	module.exports.plugins.push(
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings:     false,
				drop_console: true,
				unsafe:       true
			}
		})
	)
}
